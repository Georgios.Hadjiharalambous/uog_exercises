def add_name(l=[]):
    name=input("give a name to add\n")
    l=l.append(name)
    return l

def view_list(l=[]):
    for i,k in enumerate(l):
        print("index: ",i, "\t",k)

def delete_name(l=[]):
    view_list(l)
    i=int(input("Give me the index of the name \n"))
    del l[i]

def change_name(l=[]):
    view_list(l)
    i=int(input("Give me the index of the name \n"))
    l[i]=input("Give an aprropriate name to change to\n")
ans='1'
l=[]
while(ans!="q"):
    print("type 'a' to add another name")
    print("type 'b' to delete a name")
    print("type 'c' to change a name")
    print("type 'd' to view all names")
    print("type 'q' if you want to quit")
    ans=input("Give me an instruction\n")
    if(ans=='a'):
        add_name(l)
    if(ans=='b'):
        delete_name(l)
    if(ans=='c'):
        change_name(l)
    if(ans=='d'):
        view_list(l)
    print()
