import turtle
import random
colours=["red","blue","orange","black","white","grey"]

def square():
    turtle.shape('turtle')
    for i in range(0,4):
        ra=random.choice(colours)
        turtle.color(ra)
        turtle.forward(100)
        ra=random.choice(colours)
        turtle.color(ra)
        turtle.right(90)
    turtle.exitonclick()

def circle():
    import turtle
    t = turtle.Turtle()
    t.circle(50)


def octagon():
    turtle.shape('turtle')
    for i in range(0,8):
        ra=random.choice(colours)
        turtle.color(ra)
        turtle.forward(100)
        ra=random.choice(colours)
        turtle.color(ra)
        turtle.right(360/8)
    turtle.exitonclick()
#circle()
#square()
#octagon()

def random_shape():
    lines=random.randrange(1,20)
    for i in range(0,lines):
        len_line=random.randrange(1,100)
        angle=random.randrange(-360,360)
        turtle.shape('turtle')
        turtle.forward(len_line)
        turtle.right(angle)
    turtle.exitonclick()

random_shape()
