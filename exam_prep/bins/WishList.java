package bins;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class WishList {
	
	@Override
	public String toString() {
		String res="";
		for(BrickSet x : lista)
			res=res+(x.getNumber())+"\n";
			
		return res;//"WishList [lista=" + lista + "]";
	}
	private SortedSet<BrickSet> lista= new TreeSet<BrickSet>();
	
public Collection <BrickSet> getSets(){
	//List<BrickSet> sortedList = new ArrayList<>(lista);

	//Collections.sort(lista);	
	return lista;//new TreeSet<>(lista);
}
public boolean addSet(BrickSet set) {
	if(lista.contains(set))
		return false;
	lista.add(set);
	return true;
}

public boolean removeSet(BrickSet set) {
	if(!lista.contains(set))
		return false;
	lista.remove(set);
	return true;
}
public static void main(String[] args) {
	WishList w=new WishList();
	BrickSet x=new BrickSet(2, "name", "theme", 5);
	BrickSet x1=new BrickSet(6, "nam11e", "t44heme", 6);
	BrickSet x2=new BrickSet(22, "namerer", "t44455%%heme", 8);
	BrickSet x3=new BrickSet(-2, "namerer", "t44455%%heme", 8);

	w.addSet(x);
	w.addSet(x1);
	w.addSet(x2);
	w.addSet(x3);
	System.out.println(w.toString());

}

}
