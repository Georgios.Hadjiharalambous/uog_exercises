package bins;

import java.util.Objects;

public class BrickSet implements Comparable{
	
	private int number;
	public int getNumber() {
		return number;
	}

	public String getName() {
		return name;
	}

	public String getTheme() {
		return theme;
	}

	public int getNumberOfPieces() {
		return numberOfPieces;
	}

	public double getRetailPrice() {
		return retailPrice;
	}

	private String name;
	private String theme;
	private int numberOfPieces;
	private double retailPrice;
	public void setRetailPrice(double retailPrice) {
		this.retailPrice=retailPrice;
	}
	public double getPricePerPiece() {
		return this.retailPrice=retailPrice/numberOfPieces;
	}
	
	public BrickSet(int number, String name, String theme, int numberOfPieces) {
		super();
		this.number = number;
		this.name = name;
		this.theme = theme;
		this.numberOfPieces = numberOfPieces;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(number,name,theme,numberOfPieces,retailPrice);
	}

	
	@Override
	public String toString() {
		return "BrickSet [number=" + number + ", name=" + name + ", theme=" + theme + ", numberOfPieces="
				+ numberOfPieces + ", retailPrice=" + retailPrice + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BrickSet other = (BrickSet) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number != other.number)
			return false;
		if (numberOfPieces != other.numberOfPieces)
			return false;
		if (Double.doubleToLongBits(retailPrice) != Double.doubleToLongBits(other.retailPrice))
			return false;
		if (theme == null) {
			if (other.theme != null)
				return false;
		} else if (!theme.equals(other.theme))
			return false;
		return true;
	}

	
	

	@Override
	public int compareTo(Object a) {
		return this.number-((BrickSet) a).number;

	}

}
