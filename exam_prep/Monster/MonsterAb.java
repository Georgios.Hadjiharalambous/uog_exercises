package Monster;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class MonsterAb {


protected String type;
	protected int hitPoints;
	protected int attackPoints;
	protected String[] weekanesses;
	
	abstract protected boolean dodge();
	public MonsterAb(String type, int hitPoints, int attackPoints, String[] weekanesses) {
		this.type = type;
		this.hitPoints = hitPoints;
		this.attackPoints = attackPoints;
		this.weekanesses = weekanesses;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getHitPoints() {
		return hitPoints;
	}
	public void setHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
	}
	public int getAttackPoints() {
		return attackPoints;
	}
	public void setAttackPoints(int attackPoints) {
		this.attackPoints = attackPoints;
	}
	public String[] getWeekanesses() {
		return weekanesses;
	}
	public void setWeekanesses(String[] weekanesses) {
		this.weekanesses = weekanesses;
	}
	
	public boolean isWeakAgainst(String otherType) {
		ArrayList<String> x=new ArrayList<>(Arrays.asList(this.weekanesses));
		if(x.contains(otherType))
			return true;
		return false;
	}
	public void removeHitPoints(int pointsToRemove) {
		this.hitPoints-=pointsToRemove;
		if(this.hitPoints<0)
			this.hitPoints=0;
	}
	public boolean attack(MonsterAb otherMonster) {
		boolean flag=this.dodge();
		if(!flag) {		
		if(this==otherMonster)
			return false;
		if(this.hitPoints ==0 || otherMonster.getHitPoints()==0)
			return false;
		if(otherMonster.isWeakAgainst(this.type)) {
			otherMonster.removeHitPoints(this.attackPoints+20);
		}
		else {
			otherMonster.removeHitPoints(this.attackPoints);
		}
		return true;
		}
		else {
			this.removeHitPoints(10);
			return true;
		}
		
	}
	public static void main(String[] args) {
		Monster fireMonster = new Monster("Fire", 200, 100, new
		String[] { "Water" });
		Monster waterMonster = new Monster("Water", 130, 50, new
		String[] { "Fire", "Electric" });
		waterMonster.attack(fireMonster); // Should return true
		System.out.println(fireMonster.getHitPoints()); // Should		
		fireMonster.getHitPoints();
		}

}
