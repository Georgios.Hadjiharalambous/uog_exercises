package Monster;

public class FireMonster extends MonsterAb{
	private boolean flag=true;
	public FireMonster(String type, int hitPoints, int attackPoints, String[] weekanesses) {
		super("fire", hitPoints, attackPoints, weekanesses);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean dodge() {
		flag=!flag;
		return flag;
	}
	
	

}
