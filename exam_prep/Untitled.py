#!/usr/bin/env python
# coding: utf-8

# In[48]:


import pandas as pd
pd.DataFrame({'x':{'g':[56,8]},
             'a':{'b':[33,5]},'c':{'d':[]}})


# In[49]:


import numpy as np
v=np.array([[2],[0],[2],[0]])
print(v)

a=np.array([[23, 45, 11],[12,23,54],[29,19,34],[1,23, 10]])
print(a)

a=a+v
print(a)


# In[50]:


import numpy as np
x=np.arange(0,12).reshape(3,4)
x
x=x.ravel()
x.shape=3,4
x.shape=12
x
x=x.transpose()
x.shape
x = np.arange(30).reshape((3,10))
print(x)
[a,c,b,d,e]=np.split(x, [2,4,7,9],axis=1)
np.all(x)
x=x.ravel()
#x[0]=1
np.any(x)


# In[51]:


import numpy as np

x=np.arange(0,12)
y=2*x
#%matplotlib inline
import matplotlib.pyplot as plt
plt.subplot(221)
plt.plot(x,y,'red',marker='o')
plt.subplot(222)
plt.plot(y,y,'red',marker='+')
plt.subplot(223)

plt.plot(x,2*y,'g',marker='+')
plt.subplot(224)

plt.plot(2*x,2*y,'g',marker='^')

#plt.show()


# In[52]:


import numpy as np

x=np.arange(1,4)
y=2*x
y1=3*x
y2=3.5*x
y3=3.4*x
w=0.6
plt.bar(x,y,w/5,color='b')
plt.bar(x+w,y1,w/5,color='r')
plt.bar(x+2*w,y2,w/5,color='g')
plt.bar(x+3*w,y3,w/5,color='k')
plt.xticks(x+w/4,['b','r','g','k'])
#%matplotlib inline
import matplotlib.pyplot as plt


# In[53]:


### 
import numpy as np
k=np.ones((5,5))
k[1:-1,1:-1]=25
print(k)


# In[54]:


import numpy as  np
x1=np.array([23,45,11,5])
x2=np.array([23,5,1])
x3=np.intersect1d(x1,x2)
print(x3)


# In[55]:


x=np.arange(9).reshape(3,3)
print(x)
import os
np.savetxt('ego.txt',x,fmt="%d",header='e r p k')
a=np.loadtxt('ego.txt')
a


# In[56]:


assessment_results = {'name': ['Anastasia', 'Paul', 'Kathe', 'Joseph', 'Linda', 'Michael', 'Matt',
'Laurentine', 'Chirstian', 'Jonas'],
'score': [12.5, 10, 16.5, np.nan, 9, 20, 14.5, np.nan, 8, 19],
'attempts': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
'qualify': ['yes', 'no', 'yes', 'no', 'no', 'yes', 'yes', 'no', 'no', 'yes']}
labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']


# In[71]:


import pandas as pd
d=pd.DataFrame(assessment_results,index=labels)
x=d.iloc[:3]
x=d[d['attempts']>2]
print(d.shape[1])
print(len(d.axes[1]))

l=(d['score']<=20) & (d['score']>14)
x=d[l]
print(x)
print(d[d['score'].between(14,20)])


# In[77]:


d['score']['c']=115

print(d['score'].mean())


# In[1]:


import sqlite3
with sqlite3.connect("mpl.db") as db:
    cursor=db.cursor()
    cursor.execute("""Create table if not exists students(id integer primary key,name text,grade integer)""")
    #cursor.execute("""insert into students (id,name,grade) values(78978,"ari",123)""")
    db.commit()
    #cursor.execute("""insert into students (id,name,grade) values(22,"kiiiiii",12543)""")
    #db.commit()
    #cursor.execute("""insert into students (id,name,grade) values(?,?,?)""",(122,"kk",78))
    db.commit()
    
    cursor.execute("select * from students order by name")
    x=cursor.fetchall()
#    for i in x:
 #       print(i[2])
    k=open("edo_GRafo_basi.txt",'w')
    print(x)
    print()
    print()
    for i in x:
        k.write(str(i)+'\n')
    k.close()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    f=open("edo_GRafo_basi.txt",'r')
    print()
    print()
    l=f.read()
    print(l)
    f.close()


# In[ ]:





# In[7]:


import sqlite3
with sqlite3.connect("edo.db") as db:
    cursor=db.cursor()
    cursor.execute("Create table if not exists stude(id int primary key,name text,course text)")
    #cursor.execute("""insert into stude (id,name,course) values(1,"gio","maths")""")
    #cursor.execute("""insert into stude (id,name,course) values(2,"gesiio","al")""")
    db.commit()
    cursor.execute("Select * from stude")
    x=cursor.fetchall()
    f=open("esi.txt",'w')
    for i in x:
        f.write(str(i)+'\n')
    f.close()
    f=open("esi.csv",'w')
    for i in x:
        f.write(str(i[0])+','+str(i[1])+","+str(i[2])+'\n')
    f.close()
    f=open("esi.csv",'r')
    e=f.read()
    print(e)
    f.close()    


# In[ ]:




