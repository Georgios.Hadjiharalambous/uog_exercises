#!/usr/bin/env python
# coding: utf-8

# In[9]:


import matplotlib.pyplot as plt
plt.plot([2,4,5],linewidth=8)
plt.legend(["x"],loc=2)


# In[14]:


import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
months=mdates.MonthLocator()
days=mdates.DayLocator()
times=mdates.DateFormatter("%Y-%m")
d=[datetime.date(2020,4,4),datetime.date(2019,12,12)]
x=[2,6]
f,a=plt.subplots()
plt.plot(d,x)
a.xaxis.set_major_locator(months)
a.xaxis.set_major_formatter(times)
a.xaxis.set_minor_locator(days)


# In[4]:


#ex1
import numpy as np
import matplotlib.pyplot as plt
x=np.arange(1,30)
y=x*2
plt.title("This is a line with gradient=2")
plt.plot(x,y)


# In[37]:


#ex2
import pandas as pd
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
months=mdates.MonthLocator()
days=mdates.DayLocator()
times=mdates.DateFormatter("%Y-%m")
f1,a=plt.subplots()
df=pd.read_csv("fdata.csv")
x=pd.DataFrame(df['Date'])#,  dtype='datetime64[ns]']
x.astype('datetime64[ns]').dtypes
x=x['Date']
from datetime import datetime

#print(datetime.strptime(x[0],"%m-%d-%Y"))
#print(datetime.date(x[0]))
#u=map(datetime.date,x)
u=x
#u=list(u)
f=df.ix[:,1:]
plt.plot(u,f)
plt.legend(f,loc=2)
a.xaxis.set_major_locator(months)
a.xaxis.set_major_formatter(times)
a.xaxis.set_minor_locator(days)


# In[41]:


#ex3
x=np.arange(1,39)
x1=x*2
x2=x*3
import matplotlib.pyplot as plt
plt.plot(x,x,color='g',linewidth='1')
plt.plot(x,x1,color='r',linewidth='5')
plt.plot(x,x2,color='b',linewidth='10')
plt.legend(['x','2x','3x'])


# In[61]:


#ex4
import numpy as np
import matplotlib.pyplot as plt
x=np.arange(10,32)
yred=2*x[:10]
y=100-x[10:]*3
yg=10-3*x[:10]+60
ygg=10+2*x[10:]-40
yg=np.append(yg,ygg)

yred=np.append(yred,y)
x1=np.arange(40,10)
plt.axis([9,32,10,41])
plt.plot(x,yred,'r.')
plt.plot(x,yg,'g--',linewidth='5')
plt.legend(["red-dotted","green-dashed"])


# In[65]:


#ex5
import numpy as np
import matplotlib.pyplot as plt
x1 = [20, 30, 50, 60, 80]
y1 = [10, 50, 100, 180, 200]
x2 = [30, 40, 60, 70, 90]
y2 = [20, 60, 110, 200, 220]
#plt.plot(x1,y1)
#plt.plot(x2,y2)
plt.scatter(x1,y1)
plt.scatter(x2,y2)


# In[68]:


#ex6
import numpy as np
import matplotlib.pyplot as plt
x1 = [20, 30, 50, 60, 80]
y1 = [10, 50, 100, 180, 200]
x2 = [30, 40, 60, 70, 90]
y2 = [20, 60, 110, 200, 220]
#plt.plot(x1,y1)
#plt.plot(x2,y2)
plt.subplot(211)

plt.scatter(x1,y1)
plt.subplot(212)
plt.scatter(x2,y2,color='r')


# In[69]:


#ex7
languages= ["Python", "Java", "C", "C++", "R", "JavaScript", "C#"]
Popularity= [100, 96.3, 94.4, 87.5, 81.5, 79.4, 74.5]
import numpy as np
import matplotlib.pyplot as plt
plt.bar(languages,Popularity)


# In[71]:


#ex8
languages= ["Python", "Java", "C", "C++", "R", "JavaScript", "C#"]
Popularity= [100, 96.3, 94.4, 87.5, 81.5, 79.4, 74.5]
import numpy as np
import matplotlib.pyplot as plt
plt.barh(languages,Popularity)


# In[77]:


#ex9
languages= ["Python", "Java", "C", "C++", "R", "JavaScript", "C#"]
Popularity= [100, 96.3, 94.4, 87.5, 81.5, 79.4, 74.5]
colors=["yellow","red","blue","black","green","pink","brown"]
import numpy as np
import matplotlib.pyplot as plt
plt.bar(languages,Popularity,color=colors)


# In[108]:


#ex10


languages= ["Python", "Java", "C", "C++", "R", "JavaScript", "C#"]
Popularity= [100, 96.3, 94.4, 87.5, 81.5, 79.4, 74.5]
colors=["yellow","red","blue","black","green","pink","brown"]
import numpy as np
import matplotlib.pyplot as plt
plt.bar(languages,Popularity,color=colors)
i=0
for x in Popularity:
    i=i+1
    plt.text(i-1.2,x+2,x)


# In[110]:


#11

languages= ["Python", "Java", "C", "C++", "R", "JavaScript", "C#"]
Popularity= [100, 96.3, 94.4, 87.5, 81.5, 79.4, 74.5]
colors=["yellow","red","blue","black","green","pink","brown"]
import numpy as np
import matplotlib.pyplot as plt
plt.pie(Popularity,labels=languages,colors=colors)


# In[112]:


#12

languages= ["Python", "Java", "C", "C++", "R", "JavaScript", "C#"]
Popularity= [100, 96.3, 94.4, 87.5, 81.5, 79.4, 74.5]
colors=["yellow","red","blue","black","green","pink","brown"]
import numpy as np
import matplotlib.pyplot as plt
plt.title("languages popularity")
explode=[0,0,0,0.4,0.44,0,0.6]
plt.pie(Popularity,labels=languages,colors=colors,explode=explode)


# In[118]:


#ex13
import pandas as pd
a={'a':[10,40,39,30,39],'b':
[80,38,24,33,50],'c':
[80,36,90,25,44],'d':
[70,45,30,69,15],'e':
[25,45,39,30,55]}
a=pd.DataFrame(a)
a.plot(kind="bar")


# In[128]:


#ex14
import pandas as pd
Mean_velocity=[0.2474, 0.1235, 0.1737, 0.1824]
Std= [0.3314, 0.2278, 0.2836, 0.2645]
import matplotlib.pyplot as plt
plt.bar(['a','b','c','d'],Mean_velocity,yerr=Std)


# In[129]:


#ex15
import numpy as np
import matplotlib.pyplot as plt

N = 5
menMeans = (22, 30, 35, 35, 26)
womenMeans = (25, 32, 30, 35, 29)
menStd = (4, 3, 4, 1, 5)
womenStd = (3, 5, 2, 3, 3)
# the x locations for the groups
ind = np.arange(N)    
# the width of the bars
width = 0.35      

p1 = plt.bar(ind, menMeans, width, yerr=menStd, color='red')
p2 = plt.bar(ind, womenMeans, width,
bottom=menMeans, yerr=womenStd, color='green')

plt.ylabel('Scores')
plt.xlabel('Groups')
plt.title('Scores by group\n' + 'and gender')
plt.xticks(ind, ('Group1', 'Group2', 'Group3', 'Group4', 'Group5'))
plt.yticks(np.arange(0, 81, 10))
plt.legend((p1[0], p2[0]), ('Men', 'Women'))

plt.show()


# In[130]:


#ex16
import numpy as np
x=np.random.rand(100)
y=np.random.rand(100)
import matplotlib.pyplot as plt
plt.scatter(x,y)


# In[134]:


#ex17
import numpy as np
x=np.random.rand(100)
y=np.random.rand(100)
import matplotlib.pyplot as plt
area=np.random.rand(100)*26
plt.scatter(x,y,s=area)


# In[135]:


#ex19

import matplotlib.pyplot as plt
import pandas as pd
math_marks = [88, 92, 80, 89, 100, 80, 60, 100, 80, 34]
science_marks = [35, 79, 79, 48, 100, 88, 32, 45, 20, 30]
marks_range = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
plt.scatter(marks_range, math_marks, label='Math marks', color='r')
plt.scatter(marks_range, science_marks, label='Science marks', color='g')
plt.title('Scatter Plot')
plt.xlabel('Marks Range')
plt.ylabel('Marks Scored')
plt.legend()
plt.show()

