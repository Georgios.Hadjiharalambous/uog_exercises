import numpy as np
import matplotlib.pyplot as plt
x= ["Python", "Java", "C", "C++", "R", "JavaScript", "C#"]
popularity= [100, 96.3, 94.4, 87.5, 81.5, 79.4, 74.5]
colors=["yellow","red","blue","black","green","pink","brown"]
x_pos=[i for i,_ in enumerate(x)]
plt.bar(x_pos,popularity,color='blue')
plt.xticks(x_pos,x)
plt.minorticks_on()
plt.grid(which='major',linestyle='-',color='g')
plt.grid(which='minor',linestyle='-',color='r')
plt.show()

