OptionList = [
"red",
"black",
"brown",
"blue"
]
from tkinter import *
#Its not exactly what the exercise wants, but its better :P it could easy be transformed.
window=Tk()
window.geometry("1000x1010")
variable = StringVar(window)
variable.set(OptionList[0])
opt = OptionMenu(window, variable, *OptionList)
opt.config(width=90, font=('Helvetica', 12))
opt.pack()


def callback(*args):
    window.configure(background=variable.get())

variable.trace("w", callback)

window.mainloop()
