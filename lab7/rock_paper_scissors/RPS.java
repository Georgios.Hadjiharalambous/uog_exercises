package rock_paper_scissors;


public enum RPS {
	ROCK, PAPER, SCISSORS;
	
	GameResult getResult(RPS x) {
		if(this==x)
			return GameResult.DRAW;
		switch(this) {
		case ROCK:
		{
			switch (x) {
			case SCISSORS:
				return GameResult.WIN;
			case PAPER:
				return GameResult.LOSS;
			}			 
		}
		case SCISSORS:{
			switch (x) {
			case ROCK:
				return GameResult.LOSS;
			case PAPER:
				return GameResult.WIN;
			}
		}	
		case PAPER:{
			switch (x) {
			case SCISSORS:
				return GameResult.LOSS;
			case ROCK:
				return GameResult.WIN;
			}
		}		
		default:
			return GameResult.WIN;
		}
	}
}
 enum GameResult {
	DRAW,WIN,LOSS
}