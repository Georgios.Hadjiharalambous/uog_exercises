package rock_paper_scissors;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.Test;

class test {
	RPS[] k= {RPS.PAPER,RPS.ROCK,RPS.SCISSORS};
	Random randomno = new Random();
	int x=randomno.nextInt(3);
	RPS s=k[x];
	RPS sP=RPS.PAPER;
	RPS sR=RPS.ROCK;
	RPS sS=RPS.SCISSORS;
	@Test
	void testGetResult() {
		
		assertEquals("x not equal paper",GameResult.DRAW,sP.getResult(sP));
		assertEquals("x not equal to scissors",GameResult.DRAW,sS.getResult(sS));
		assertEquals("x not equal to rock",GameResult.DRAW,sR.getResult(sR));
		assertEquals("paper wins rock",GameResult.WIN,sP.getResult(sR));
		assertEquals("rock loss to paper",GameResult.LOSS,sR.getResult(sP));
		assertEquals("paper loss to scissors",GameResult.LOSS,sP.getResult(sS));
		assertEquals("scissors win paper",GameResult.WIN,sS.getResult(sP));
		assertEquals("scissors loss to rock",GameResult.LOSS,sS.getResult(sR));

		assertEquals("rock wins scissors",GameResult.WIN,sR.getResult(sS));


	}

}
