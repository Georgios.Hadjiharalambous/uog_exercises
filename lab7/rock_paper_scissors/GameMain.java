package rock_paper_scissors;

import java.util.Scanner;

public class GameMain {
	
	public static void main(String[] args) {
		int wins=0;
		GamePlayer gp=new GamePlayer();
		System.out.println("Give the number of rounds you want to play");
		Scanner input = new Scanner(System.in);
		int number = input.nextInt();
		for (int i=0;i<number;i++) {
			RPS s=gp.getSymbol();
			System.out.println("Insert number for the symbol you want 1=PAPER, 2= SCISSORS, 3=ROCK ");
			int number1 = input.nextInt();
			RPS userS=gp.k[number1-1];
			GameResult res=userS.getResult(s);
			System.out.println("The result for you is "+res);
			if(res==GameResult.WIN)
				wins++;
		}
		System.out.format("You have won %d out %d times\n",wins ,number);
		System.out.format("The computer won %d out %d times",number - wins ,number);

	}

}
