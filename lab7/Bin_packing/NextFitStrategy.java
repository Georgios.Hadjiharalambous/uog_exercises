package Bin_packing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class NextFitStrategy implements PackingStrategy {

	boolean flag=true;
	
	public LinkedHashSet<Bin> pack(int capacity, List<Integer> values) {
		Bin b=new Bin(capacity);
		LinkedHashSet<Bin> result=new LinkedHashSet<Bin>();
		result.add(b);
		for(int x : values) {
			if(b.getSpace()>=x)
				b.store(x);
			else {
				b= new Bin(capacity);
				result.add(b);
				b.store(x);
			}			
		}
		return result;
	}
	public List<Integer> getList(int capacity,List<Integer> values){
		
		LinkedHashSet<Bin> result=pack(capacity,values);//new HashSet<Bin>();
		List<Integer> x=new ArrayList<Integer>();
		
		for(Bin b:result) 
			x.add(b.capacity-b.getSpace());
		return x;
	}

}
