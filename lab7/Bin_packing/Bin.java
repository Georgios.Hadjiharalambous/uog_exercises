package Bin_packing;

import java.util.ArrayList;

public class Bin {
	
	
	int capacity;
	ArrayList<Integer> weights=new ArrayList<Integer>();
	
	Bin(int cap){
		this.capacity=cap;
	}
	
	
	public void store(int weight) throws IllegalArgumentException{
		int left=this.getSpace();
		if(left<weight)
			throw new IllegalArgumentException("store not possible");
		else {
			this.weights.add(weight);
		}	
	}
	public int getSpace() {
		int sum=0;		
		for(int x:this.weights) {
			sum=sum+x;
		}
		return this.capacity-sum;
		
	}
	

}
