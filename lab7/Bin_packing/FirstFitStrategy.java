package Bin_packing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class FirstFitStrategy implements PackingStrategy {

	@Override
	public LinkedHashSet<Bin> pack(int capacity, List<Integer> values) {
		Bin b=new Bin(capacity);
		LinkedHashSet<Bin> result=new LinkedHashSet<Bin>();
		result.add(b);
		boolean flag=false;
		for(int x:values) {
			flag=false;
			for(Bin bin: result) {
				if(bin.getSpace()>=x) {
					bin.store(x);
					flag=true;//have already placed the weight in a bin.
					break;
				}				
			}
			
			if(!flag) {
				b=new Bin(capacity);
				result.add(b);
				if(b.getSpace()>x) {
					b.store(x);
				}
			}
		}
		return result;
	}	
	
	public List<Integer> getList(int capacity,List<Integer> values){
		LinkedHashSet<Bin> result=pack(capacity,values);//new HashSet<Bin>();
		List<Integer> x=new ArrayList<Integer>();		
		for(Bin b:result)
			x.add(b.capacity-b.getSpace());		
		return x;
	}
}
