package Bin_packing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class BestFitStrategy implements PackingStrategy {

	@Override
	public LinkedHashSet<Bin> pack(int capacity, List<Integer> values) {
		Bin b=null;//=new Bin(capacity);
		LinkedHashSet<Bin> result=new LinkedHashSet<Bin>();
		boolean flag=false;
		int min=capacity;
		for(int x :values) {
			min=capacity;
			flag=false;
			for (Bin bin:result) {
				if(bin.getSpace()>=x & bin.getSpace()-x<min) {
					b=bin;
					min=bin.getSpace()-x;			
					flag=true;
				}	
			}
					
			if(!flag) {//create new object
				b=new Bin(capacity);
				result.add(b);
				b.store(x);
			}
			else {
			b.store(x);	
			}
		}
			return result;
	}
	public List<Integer> getList(int capacity,List<Integer> values){
		LinkedHashSet<Bin> result=pack(capacity,values);//new HashSet<Bin>();
		List<Integer> x=new ArrayList<Integer>();
		
		for(Bin b:result)
			x.add(b.capacity-b.getSpace());
		return x;
	}

}
