package Bin_packing;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class TestNext {

	List<Integer> values=Arrays.asList(75,50,20,60,40,50);
	 List<Integer> solution=Arrays.asList(75,70,100,50);
	 NextFitStrategy strat=new NextFitStrategy();
	@Test
	void testGetList() {
		System.out.println("next fit");
		System.out.println(strat.getList(100, values));
		System.out.print(solution);
		assertEquals("next fit",solution,strat.getList(100, values));
	}

}
