package Bin_packing;

import java.util.Set;

import java.util.List;

public interface PackingStrategy {

	 public Set<Bin> pack(int capacity,List<Integer> values);
}
