
public class Monster {
	
	private String type;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getHit_points() {
		return hit_points;
	}
	public void setHit_points(int hit_points) {
		this.hit_points = hit_points;
	}
	public int getAttack_points() {
		return attack_points;
	}
	public void setAttack_points(int attack_points) {
		this.attack_points = attack_points;
	}
	public String[] getWeaknesses() {
		return weaknesses;
	}
	public void setWeaknesses(String[] weaknesses) {
		this.weaknesses = weaknesses;
	}
	private int hit_points;
	private int attack_points;
	private String[] weaknesses;
	
	public boolean isWeakAgainst(String otherType) {
		for(String x : this.weaknesses) {
			if (x.equals(otherType))
			return true;
			
		}
		return false;
	}
	
	public void removeHitPoints(int pointsToRemove) {
		this.hit_points-=pointsToRemove;
		if(this.hit_points<0)
			this.hit_points=0;
	}
	public boolean attack(Monster otherMonster) {
		if(otherMonster==this)
			return false;
		if(otherMonster.hit_points==0 | this.hit_points==0) 
			return false;
		
		boolean flag=isWeakAgainst(otherMonster.type) ;
		if(flag) 
			otherMonster.removeHitPoints(this.attack_points);
		else
			this.removeHitPoints(otherMonster.attack_points+20);	
		
		return flag;
	}
	
	

}
